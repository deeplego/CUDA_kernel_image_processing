<img src="/report/figures/2-img.jpg"  width="356" height="200" title="original">
<img src="/report/figures/2-img_EdgeDetection_tiled.jpg"  width="356" height="200" title="edge detection">
<img src="/report/figures/2-img_GaussianBlur_tiled.jpg"  width="356" height="200" title="gaussian blur">

(move the cursor over an image to know the effect)

### Download the test images
All the test images can be downloaded with the following command:

```bash
$ cd ./data
$ ./download_images.sh
```

# How to replicate a specific experiment:
In order to replicate a specific experiment you can do the following:

### Choose an image
Edit line 50 with the right filename,
e.g. "../data/2-img.ppm".

### Choose a filter
Edit line 53-56 to choose a filter among the following:  
    - Edge detection;  
    - Gaussian Blur;  
    - Sharpen.  

### Choose a dimension of the thread block (it is assumed square):
Edit line 18 (TILE_WIDTH variable).

### Choose k of the mask dimensions (2k + 1)x(2k + 1):
Edit line 19 (MASK_WIDTH variable).