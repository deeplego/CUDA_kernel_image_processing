#!/usr/bin/env bash

VAR1=${1:-10}

for i in $(seq 1 ${VAR1})
do
./CUDA_kernel_image_processing | python ./redOut.py
done
