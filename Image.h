//
// Created by lore on 3/21/19.
//

#ifndef IMAGE_H
#define IMAGE_H

typedef struct {
    int width;
    int height;
    int nChannels;
    int depth;
    float *data;
} Image_t;

inline int Image_getWidth(const Image_t *img) { return img->width; }
inline int Image_getHeight(const Image_t *img) { return img->height; }
inline int Image_getNChannels(const Image_t *img) { return img->nChannels; }
inline int Image_getDepth(const Image_t *img) { return img->depth; }
inline float* Image_getData(const Image_t *img) { return img->data; }

inline void Image_setWidth(Image_t *img, int width) { img->width = width; }
inline void Image_setHeight(Image_t *img, int height) { img->height = height; }
inline void Image_setNChannels(Image_t *img, int nChannel) { img->nChannels = nChannel; }
inline void Image_setDepth(Image_t *img, int depth) { img->depth = depth; }
inline void Image_setData(Image_t *img, float *data) { img->data = data; }

Image_t* Image_read(const char *filename);
void Image_write(Image_t *img, const char *filename);
Image_t* Image_new(int width, int height, int nChannel, int depth, float *data);
void Image_delete(const Image_t *img);

void Image_showProperties(const Image_t *img);

#endif //IMAGE_H