//
// Created by lore on 3/21/19.
//

#include "Image.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <boost/algorithm/string.hpp>

Image_t* Image_new(int width, int height, int nChannels, int depth, float *data) {
    Image_t *img = new Image_t;

    Image_setWidth(img, width);
    Image_setHeight(img, height);
    Image_setNChannels(img, nChannels);
    Image_setDepth(img, depth);
    Image_setData(img, data);

    return img;
}

void Image_delete(const Image_t *img) {
    if (img != nullptr) {
        if (Image_getData(img) != nullptr)
            delete[] Image_getData(img);
        delete img;
    }
}

Image_t* Image_read(const char *filename) {
    std::ifstream infile;
    infile.open(filename);
    Image_t *img = nullptr;

    try {
        if ( infile.fail() )
            throw "The file cannot be opened.";

        std::string line;
        getline(infile, line);
        while (line[0] == '#')
            getline(infile, line);
        if ( line != "P6" )
            throw "The file must be in .ppm format.";
        int nChannels = 3;

        getline(infile, line);
        while (line[0] == '#')
            getline(infile, line);
        std::vector<std::string> w_h;
        boost::split(w_h, line, [](char c) { return c == ' '; });

        int width = std::stoi( w_h.at(0) );
        int height = std::stoi( w_h.at(1) );

        getline(infile, line);
        while (line[0] == '#')
            getline(infile, line);
        float depth = std::stof(line);

        float *data = new float[width * height * nChannels];
        unsigned char dataBuff[1];

        for (int i = 0; i < width * height * nChannels; ++i) {
            infile.read(reinterpret_cast<char *>(dataBuff), 1);
            data[i] = dataBuff[0] / depth;
        }
//        infile.read(dataBuff, width * height * nChannels);
//        for (int i = 0; i < 300; ++i) {
//            std::cout << data[i] << ' ';
//        }

//        infile.read(dataBuff, width * height * nChannels);
//        std::cout << dataBuff[0] << dataBuff[299] << dataBuff[300] << '\n';
//        for (int i = 0; i < width * height * nChannels; ++i) {
//            data[i] = dataBuff[i] / depth;
//        }

        infile.close();
        img = Image_new(width, height, nChannels, static_cast<int>(depth), data);

    } catch (const char *err) {
        std::cerr << err << '\n';
        infile.close();
        exit(1);
    }
    return img;
}

void Image_write(Image_t *img, const char *filename) {
    std::ofstream outfile;
    outfile.open(filename);
    try {
        if ( outfile.fail() )
            throw "The file cannot be opened.";

        outfile << "P6\n" << Image_getWidth(img) << ' '
                << Image_getHeight(img) << '\n' << Image_getDepth(img) << '\n';

        float depth = static_cast<float>( Image_getDepth(img) );
        for (int i = 0; i < Image_getWidth(img) * Image_getHeight(img) * Image_getNChannels(img); ++i) {
            outfile << static_cast<unsigned char>( std::min(std::max(Image_getData(img)[i], 0.f), 1.f) * depth );
        }
        outfile.close();

    } catch (const char *err) {
        std::cerr << err << '\n';
        outfile.close();
    }
}

void Image_showProperties(const Image_t *img) {
    std::cout << "Width: " << Image_getWidth(img) << '\n';
    std::cout << "Height: " << Image_getHeight(img) << '\n';
    std::cout << "Depth: " << Image_getDepth(img) << '\n';
    std::cout << "nChannels: " << Image_getNChannels(img) << '\n';
}