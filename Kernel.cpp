//
// Created by lore on 4/1/19.
//

#include "Kernel.h"

void Kernel::print() const {
    for (int i = 0; i < maskWidth; ++i) {
        for (int j = 0; j < maskWidth; ++j) {
            std::cout << mask[i * maskWidth + j] << ' ';
        }
        std::cout << '\n';
    }
}

Kernel::~Kernel() {
    delete[] mask;
}

bool Kernel::isFlippable() const {
    // check if the rows and columns of the kernel can be flipped without change.
    float eps = 1e-7;
    auto *temp = new float[maskWidth * maskWidth];

    int rowTemp = maskWidth - 1;
    for (int i = 0; i < maskWidth; ++i) {
        int colTemp = maskWidth - 1;
        for (int j = 0; j < maskWidth; ++j) {
            temp[i * maskWidth + j] = mask[rowTemp * maskWidth + colTemp];
            --colTemp;
        }
        --rowTemp;
    }

    for (int i = 0; i < maskWidth * maskWidth; ++i) {
        if ( std::abs(temp[i] - mask[i]) > eps ) {
            delete[] temp;
            return false;
        }
    }
    delete[] temp;
    return true;
}

std::ostream &operator<<(std::ostream &out, Kernel *kernel) {
    kernel->print();
    return out;
}

BoxBlur::BoxBlur(int p_maskWidth, const char *p_name):
        Kernel(p_maskWidth, p_name) {
    mask = new float[p_maskWidth * p_maskWidth];
    const float normalization = p_maskWidth * p_maskWidth;

    for (int i = 0; i < p_maskWidth; ++i) {
        for (int j = 0; j < p_maskWidth; ++j) {
            mask[i * p_maskWidth + j] = 1 / normalization;
        }
    }
}

GaussianBlur::GaussianBlur(float p_stddev, int p_maskWidth, const char *p_name) :
        Kernel(p_maskWidth, p_name) {
    stddev = p_stddev;
    mask = new float[maskWidth * maskWidth];
    float normalization = 0;
    for (int i = -maskWidth / 2; i < maskWidth / 2 + 1; ++i) {
        for (int j = -maskWidth / 2; j < maskWidth / 2 + 1; ++j) {
            const int row = i + maskWidth / 2;
            const int col = j + maskWidth / 2;
            auto temp = static_cast<float>( 1 / (2 * pi * std::pow(stddev, 2)) * std::exp( - (pow(i, 2) + std::pow(j, 2)) / (2 * std::pow(stddev, 2)) ) );
//            normalization += temp;  // Uncomment to normalize the mask. In the case uncomment the application of the normalization too.
            mask[row * maskWidth + col] = temp;
        }
    }
//    for (int i = 0; i < maskWidth * maskWidth; ++i)
//        mask[i] /= normalization;
}

EdgeDetection::EdgeDetection(int p_maskWidth, const char *p_name) : Kernel(p_maskWidth, p_name) {
    mask = new float[maskWidth * maskWidth];
    for (int i = 0; i < maskWidth * maskWidth; ++i) {
        mask[i] = -1;
    }
    mask[(maskWidth / 2) * maskWidth + (maskWidth / 2)] = maskWidth * maskWidth - 1;
}
